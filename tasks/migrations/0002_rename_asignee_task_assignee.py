# Generated by Django 4.0.3 on 2022-03-29 17:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("tasks", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="task",
            old_name="asignee",
            new_name="assignee",
        ),
    ]
